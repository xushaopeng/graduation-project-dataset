# 静态分析使用的数据集

- ifttt_201705: 从ifttt网站爬取的初始数据集
- triggerList.json: 过滤得到的225种trigger类型
- actionList.json: 过滤得到的189种action类型
- all.json: 过滤得到的4961条规则
- all.yaml: all.json转换得到的yaml格式的数据